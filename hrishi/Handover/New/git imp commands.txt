>> initiate git
	git init  # initiate with non-bare repository
	git init --bare # initiate with bare repository
	
>> Create .gitignore file to have all the folders and files need not to be added.
	cat .gitignore
	
>> Add all files from need to be added to git
	git add .
	
>> Commit the changes made with the messages.
	git commit -m "Messages"

>>To clone repo from remote server	
	git clone git@192.168.100.187:/home/git/src/

>>To add branch to remote repository
	git remote add origin git@192.168.100.187:/home/git/src

>>Push data to origin master
	git push origin master

>>To remove any branch
	git remote rm origin
	
>>To pull data from origin master
	git pull origin master
	
>>A .gitignore file only affects untracked files that you don't want to add to the repository.
If a file is already tracked in your repository, In that case pushing and pulling changes to it will not be affected by the .gitignore file, 

If you didn't want to add this file to the repository, you can use git rm --cached -r resources crop.html
 to remove the file from the index, but keep it around locally.


Error: Your local changes to the following files would be overwritten by merge:
	For this run below command...
		 git checkout HEAD <FileName>







#######################################################################################################################################################

git merge strategies


git merge can have conflicts due to more than one developers working in the same project. Or it can give conflicts to the binary files which are not included in gitignore


--> Merging code while pulling from remote server
 	git pull -X ours or theirs origin <branchname>
	

--> Merging two branches in our local repo
	git checkout branchA
	git merge -X ours or theirs branchB 


