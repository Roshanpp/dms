# Fetching account Number from Database
import requests
import os
import json
import shutil
import datetime

from data import DMS_ACCOUNT_FETCH_URL, ACCOUNT_NUMBER_FILE, FINAL_DOWNLOAD_PATH, LOG_DIRECTORY, INITIAL_DOWNLOADED_PATH, UPLOAD_LOG_DIRECTORY, DOWNLOAD_LOG_DIRECTORY

CHECK_FOLDER = os.path.isdir(LOG_DIRECTORY)
CHECK_FOLDER2 = os.path.isdir(INITIAL_DOWNLOADED_PATH)
CHECK_FOLDER3 = os.path.isdir(FINAL_DOWNLOAD_PATH)
CHECK_FOLDER4 = os.path.isdir(UPLOAD_LOG_DIRECTORY)
CHECK_FOLDER5 = os.path.isdir(DOWNLOAD_LOG_DIRECTORY)

if not CHECK_FOLDER:
	os.makedirs(LOG_DIRECTORY)
	print(LOG_DIRECTORY, 'created')
	
if not CHECK_FOLDER2:
	os.makedirs(FINAL_DOWNLOAD_PATH)
	print(FINAL_DOWNLOAD_PATH, 'created')

if not CHECK_FOLDER3:
	os.makedirs(INITIAL_DOWNLOADED_PATH)
	print(INITIAL_DOWNLOADED_PATH, 'created')


if not CHECK_FOLDER4:
	os.makedirs(UPLOAD_LOG_DIRECTORY)
	print(UPLOAD_LOG_DIRECTORY, 'created')	

if not CHECK_FOLDER5:
	os.makedirs(DOWNLOAD_LOG_DIRECTORY)
	print(DOWNLOAD_LOG_DIRECTORY, 'created')		
	

urls = DMS_ACCOUNT_FETCH_URL 

r = requests.post(urls)
print(r.text)

data_account_number = json.loads(r.text)

account_number_file = ACCOUNT_NUMBER_FILE

if not os.path.isfile(account_number_file):
	f = open(account_number_file, "w+")
	f.close()
	

file = open(account_number_file,"r+")
file.truncate(0)
file.close()

for i, j in data_account_number.items():
	if j:
		for acc in j:
			with open(account_number_file, 'a+') as account_number:
				account_number.write(acc)
				account_number.write('\n')
			
			
# Account Numbers Fetched
print("Account Numbers Fetched from database")



for filename in os.listdir(FINAL_DOWNLOAD_PATH):
	file_path = os.path.join(FINAL_DOWNLOAD_PATH, filename)
	try:
		if os.path.isfile(file_path) or os.path.islink(file_path):
			os.unlink(file_path)
		elif os.path.isdir(file_path):
			shutil.rmtree(file_path)
	except Exception as e:
		print('Failed to delete %s. Reason: %s' % (file_path, e))


# Download document script
import bs4
import requests
from urllib.parse import urlencode
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium import webdriver
import time
import shutil
import os
import logging
from itertools import islice

from data import LOG_FILENAME, LOGIN_URL, ACCOUNT_NUMBER_FILE, INITIAL_DOWNLOADED_PATH, FINAL_DOWNLOAD_PATH, USERNAME, PASSWORD
from data import ACCESS_UPLOAD_REPORT_FILENAME, ACCESS_DOWNLOAD_REPORT_FILENAME, ERROR_UPLOAD_REPORT_FILENAME, ERROR_DOWNLOAD_REPORT_FILENAME
from data import DMS_ACCOUNT_UPLOAD_URL

upload_urls = DMS_ACCOUNT_UPLOAD_URL

LOG_FILENAME = LOG_FILENAME

LOGIN = LOGIN_URL

ACCOUNT_NUMBER_FILENAME = ACCOUNT_NUMBER_FILE
ACCESS_UPLOAD_REPORT_FILENAME = ACCESS_UPLOAD_REPORT_FILENAME
ACCESS_DOWNLOAD_REPORT_FILENAME = ACCESS_DOWNLOAD_REPORT_FILENAME
ERROR_UPLOAD_REPORT_FILENAME = ERROR_UPLOAD_REPORT_FILENAME

if not os.path.isfile(ACCESS_UPLOAD_REPORT_FILENAME):
	f = open(ACCESS_UPLOAD_REPORT_FILENAME, "w+")
	f.close()
	
if not os.path.isfile(ACCESS_DOWNLOAD_REPORT_FILENAME):
	f = open(ACCESS_DOWNLOAD_REPORT_FILENAME, "w+")
	f.close()
	
if not os.path.isfile(ERROR_UPLOAD_REPORT_FILENAME):
	f = open(ERROR_UPLOAD_REPORT_FILENAME, "w+")
	f.close()
	
if not os.path.isfile(ERROR_DOWNLOAD_REPORT_FILENAME):
	f = open(ERROR_DOWNLOAD_REPORT_FILENAME, "w+")
	f.close()
	
	

INITIAL_DOWNLOADED_PATH = INITIAL_DOWNLOADED_PATH
FINAL_DOWNLOAD_PATH = FINAL_DOWNLOAD_PATH

USERNAME = USERNAME
PASSWORD = PASSWORD

profile = webdriver.FirefoxProfile()
profile.set_preference("browser.download.folderList", 2)
profile.set_preference("browser.download.dir", INITIAL_DOWNLOADED_PATH)
profile.set_preference("browser.download.manager.showWhenStarting", False)
profile.set_preference("browser.helperApps.neverAsk.saveToDisk", "image/tiff, image/tif , application/msword, application/csv, application/ris, text/csv, image/png, application/pdf, text/html, text/plain, application/zip, application/x-zip, application/x-zip-compressed, application/download, application/octet-stream")
profile.set_preference("browser.helperApps.neverAsk.openFile", "image/tiff, image/tif , application/msword, application/csv, application/ris, text/csv, image/png, application/pdf, text/html, text/plain, application/zip, application/x-zip, application/x-zip-compressed, application/download, application/octet-stream")
profile.set_preference("browser.helperApps.alwaysAsk.force", False)
profile.set_preference("browser.download.manager.useWindow",False)
profile.set_preference("browser.download.useDownloadDir",True)
profile.set_preference("browser.download.manager.focusWhenStarting",False)
profile.set_preference("browser.download.manager.alertOnEXEOpen",False)
profile.set_preference("browser.download.manager.showAlertOnComplete",False)
profile.set_preference("browser.download.manager.closeWhenDone",True)
profile.set_preference("services.sync.prefs.sync.browser.download.manager.showWhenStarting", False)
profile.set_preference("pdfjs.disabled", True)
profile.set_preference("browser.download.panel.shown", False)

firefox_capabilities = webdriver.DesiredCapabilities.FIREFOX
firefox_capabilities['marionette'] = True

binary = FirefoxBinary('C:\\Program Files\\Mozilla Firefox\\firefox.exe')
options = Options()
#prefs = {'download.default_directory': 'D:\\COSMOS\\Downloads'}    # Change this according to requirement
options.add_argument('--headless')

#caps = webdriver.DesiredCapabilities().FIREFOX
#caps["marionette"] =True
#ff_profile = webdriver.FirefoxProfile()
driver = webdriver.Firefox(capabilities=firefox_capabilities, firefox_profile=profile, firefox_binary=binary, executable_path='D:\\DMS\\Script\\geckodriver.exe')   # Where the chromedriver/firefoxdriver is located
driver.maximize_window()

driver.get(LOGIN)
# Login Page
username = driver.find_element_by_id("userName")
password = driver.find_element_by_id("password")

username.send_keys(USERNAME)
password.send_keys(PASSWORD)

driver.find_element_by_class_name('LoginButton').click()
time.sleep(2)

try:
	alert = driver.switch_to.alert
	alert.accept()
	print("Another user logged in prompt opened and accepted")
	time.sleep(2)
except:
	print("No alert")

time.sleep(2)
# First Page after login
#driver.find_element_by_link_text("MasterDesktop").click()
driver.find_element_by_xpath("/html/body/div[1]/table/tbody/tr[1]/td[6]/a").click()

time.sleep(2)

window_before = driver.window_handles[0]
print(window_before)

time.sleep(8)
iframe = driver.find_element_by_name('frmTop')
driver.switch_to.frame(iframe)
time.sleep(4)
search_item = driver.find_element_by_xpath('//html//body//div[1]//table//tbody//tr//td[2]//div//center//table//tbody//tr//td[9]//div//center//p//a')
search_item.click()

time.sleep(2)

window_after = driver.window_handles[1]
print(window_after)

driver.switch_to.window(window_after)

#Search Page after Login
with open(ACCOUNT_NUMBER_FILENAME, encoding='utf-8-sig') as in_file:
	for f_name in in_file:
		#delete all files inside the INITIAL_DOWNLOADED_PATH
		for filename in os.listdir(INITIAL_DOWNLOADED_PATH):
			file_path = os.path.join(INITIAL_DOWNLOADED_PATH, filename)
			try:
				if os.path.isfile(file_path) or os.path.islink(file_path):
					os.unlink(file_path)
				elif os.path.isdir(file_path):
					shutil.rmtree(file_path)
			except Exception as e:
				print('Failed to delete %s. Reason: %s' % (file_path, e))
	
		print("Account Number == " + f_name)
		logging.info("Process for "+"Account Number == " + f_name)
		
		# Writing into a file == Account Number
		# f = open(DOWNLOAD_REPORT_FILENAME, "a+")
		# f.write("<" + str(datetime.datetime.now()) + "> " + "Account Number == " + f_name)
		# f.close()
		
		time.sleep(1)
		first_iframe = driver.find_element_by_name('frmSearch')
		driver.switch_to.frame(first_iframe)
		
		time.sleep(1)
		sec_iframe = driver.find_element_by_name('frmFolderSearchMain')
		driver.switch_to.frame(sec_iframe)
		time.sleep(2)
		search_item_sec = driver.find_element_by_name("folderName")
		search_item_sec.clear()
		search_item_sec.send_keys(f_name)
		time.sleep(2)
		
		#Switching back to Main Html
		driver.switch_to.default_content()
		
		driver.switch_to.frame(first_iframe)
		
		# Hitting Search Button on search Page
		search_iframe = driver.find_element_by_name('frmFolderSearchOpt')
		driver.switch_to.frame(search_iframe)
		search_button = driver.find_element_by_name('B1')
		search_button.click()
		time.sleep(2)
		
		#Switching back to Main Html
		driver.switch_to.default_content()
		driver.switch_to.frame(first_iframe)
		time.sleep(1)
		
		third_iframe = driver.find_element_by_name('frmFolderSearchResult')
		driver.switch_to.frame(third_iframe)
		time.sleep(1)
		
		fourth_iframe = driver.find_element_by_name('frmFolderSearchResultMain')
		driver.switch_to.frame(fourth_iframe)
		time.sleep(2)
		try:
			#elems = find_element_by_xpath("//a[contains(text(), 'dmsprod/') and contains(text(), 'Catalogues')]")
			row_count = list(driver.find_elements_by_xpath("/html/body/form[1]/table/tbody/tr"))
			print('Length of list is', str(int(len(row_count)-1)))
			time.sleep(1)
			logging.info("Total number of accounts for this account is " + str(int(len(row_count)-1)))
			
			iter_row_count = iter(row_count)
			next(iter_row_count)
			for i, item in enumerate(iter_row_count):
				print('count', i)
				count_number =  int(int(i) + 2)
				clickable_link = driver.find_element_by_xpath('/html/body/form[1]/table/tbody/tr['+str(count_number)+']/td[2]/a[2]')
				time.sleep(1)
				# Text Attribute
				attribute_value = clickable_link.text
				print(attribute_value)
				
				value_for_folder = attribute_value.split('/')[1]  # Folder name we need to create
				print(value_for_folder)
				clickable_link.click()
				
				driver.switch_to.window(window_before)
				time.sleep(2)
				
				# Page after link is clicked
				# Switching back to Main Html
				driver.switch_to.default_content()
				
				fifth_iframe = driver.find_element_by_name('frmdoclist')
				driver.switch_to.frame(fifth_iframe)
				time.sleep(1)
				
				sixth_iframe = driver.find_element_by_name('frmdoclistmain') 
				driver.switch_to.frame(sixth_iframe)
				time.sleep(1)
				
				try:
					select_all = driver.find_element_by_name("selAll")
					select_all.click()
					
					#Switching back to Main Html
					driver.switch_to.default_content()
					driver.switch_to.frame(fifth_iframe)
					
					seventh_iframe = driver.find_element_by_name('frmdoclistopt')
					driver.switch_to.frame(seventh_iframe)
					time.sleep(1)
					
					download_link = driver.find_element_by_xpath('/html/body/table/tbody/tr[1]/td[8]/a')
					download_link.click()
					
					
					for handle in driver.window_handles:
						if handle != window_after and handle != window_before:
							driver.switch_to.window(handle)
							time.sleep(1)
							extra_download_button = driver.find_element_by_xpath('/html/body/form[1]/table[5]/tbody/tr/td/input[1]')
							extra_download_button.click()
						else:
							pass
								
					time.sleep(3)
					time.sleep(3)
					
					
					#Wait until the file is fully downloaded
					latest_filename = max([INITIAL_DOWNLOADED_PATH + "\\" + f for f in os.listdir(INITIAL_DOWNLOADED_PATH)], key=os.path.getctime)

					while not os.path.exists(os.path.join(INITIAL_DOWNLOADED_PATH, latest_filename)):
						print(os.path.exists(os.path.join(INITIAL_DOWNLOADED_PATH, latest_filename)), " Path does not exist, waiting for download")
						time.sleep(1)
						
					if os.path.exists(os.path.join(INITIAL_DOWNLOADED_PATH, latest_filename)):
						print("File Downloaded")
						
					
					time.sleep(4)
					logging.info("File Downloaded for account " + f_name + " for account type" + value_for_folder)
					
					
					f = open(ACCESS_DOWNLOAD_REPORT_FILENAME, "a+")
					f.write("<" + str(datetime.datetime.now()) + "> " + "Account: " + str(f_name).rstrip('\n') + " Type: " + value_for_folder + " == Downloaded")
					f.write("\n")
					f.close()
					
					account_number = str(f_name).rstrip('\n')
					
					if not os.path.isdir(FINAL_DOWNLOAD_PATH + '\\' + account_number):
						print(account_number +  ' path does not exists')
						try:
							path_new = os.path.join(FINAL_DOWNLOAD_PATH + '\\' + account_number)
							os.makedirs(path_new)
						except OSError as error: # Guard against race condition
							print("Directory" + account_number + "can not be created" + str(error))
					

					value_path = os.path.join(FINAL_DOWNLOAD_PATH + '\\' +  account_number + '\\' + value_for_folder + '_' + str(int(i + 1)))
					
					if not os.path.isdir(FINAL_DOWNLOAD_PATH + account_number + '\\' + value_for_folder + '_' + str(int(i + 1))):
						print(value_for_folder + " Path doesn't exists")
						try:
							os.makedirs(value_path)
							print('Path created', value_path)	
						except OSError as error:
							print("Directory" + value_for_folder + '_' + str(int(i + 1)) + " can not be created" + str(error))
					
					try:
						filename = max([INITIAL_DOWNLOADED_PATH + "\\" + f for f in os.listdir(INITIAL_DOWNLOADED_PATH)], key=os.path.getctime)
						print("Latest filename" + filename)
						striped_filename = filename.split("\\")[-1]
						print("Striped filename" + striped_filename)
						#Moving documents to its respective folder   
					
						downloaded_path = os.path.join(INITIAL_DOWNLOADED_PATH + "\\" + striped_filename)
						new_path_for_file = os.path.join(value_path + '\\' + striped_filename)
						
						try:
							shutil.move(downloaded_path, new_path_for_file)
							print(filename + ' File Moved to ' + ' ' + new_path_for_file )
						except OSError as error:
							print(filename + " File can't be moved ", error)
						time.sleep(3)
						
						try:
							
							account_type = value_for_folder + '_' + str(int(i + 1))
							
							PARAMS = {'l_acc_no': account_number, 'l_acc_type': account_type }
							fo = open(new_path_for_file, 'rb')
							files = {'files': fo}
						
							r = requests.post(urls,  data=PARAMS, files=files)
							
							r.text
							
							f = open(ACCESS_UPLOAD_REPORT_FILENAME, "a+")
							f.write("<"+ str(datetime.datetime.now())+ "> "+ "Account: " + l_acc_no + ' Type: ' + l_acc_type + " == Upload Successfull")
							f.write("\n")
							f.close()
							
							print("Account", l_acc_no, ", ",  l_acc_type, "Uploaded successfully")
							
							fo.close()
							
							
							# deleting files after Upload
							
							for filename in os.listdir(FINAL_DOWNLOAD_PATH):
								file_path = os.path.join(FINAL_DOWNLOAD_PATH, filename)
								try:
									if os.path.isfile(file_path) or os.path.islink(file_path):
										os.unlink(file_path)
									elif os.path.isdir(file_path):
										shutil.rmtree(file_path)
								except Exception as e:
									print('Failed to delete %s. Reason: %s' % (file_path, e))
							
						except:
							print(" No such directory with", account_number, "/", account_type)
							f = open(ERROR_UPLOAD_REPORT_FILENAME, "a+")
							f.write("<"+ str(datetime.datetime.now())+ "> "+ " No such directory with "+ account_number+ "/"+ account_type)
							f.write("\n")
							f.close()
							
							
					except OSError as error:
						print("No Such latest File " + error)
						f = open(ERROR_DOWNLOAD_REPORT_FILENAME, "a+")
						f.write("<" + str(datetime.datetime.now()) + "> " + "Account: " + str(f_name).rstrip('\n') + " Type: " + value_for_folder + " ==  No Such latest File " + error)
						f.write("\n")
						f.close()
					 
				except:
					print(" No Files are present for that Account == " + f_name + " " + value_for_folder + '_' + str(int(i + 1)))
					print("\n")
					
					# Writing into a file == Not completed
					f = open(ERROR_DOWNLOAD_REPORT_FILENAME, "a+")
					f.write("<" + str(datetime.datetime.now()) + "> " + "Account: "+ str(f_name).rstrip('\n') +  " Type: " + value_for_folder + " ==  Not Completed  -- Error Occurred")
					f.write("\n")
					f.close()
				
				driver.switch_to.window(window_after)
				time.sleep(2)
				
		except:
			print("No Such Element Found")
			f = open(ERROR_DOWNLOAD_REPORT_FILENAME, "a+")
			f.write("<"+ str(datetime.datetime.now())+ "> "+ "Account Number: " + str(f_name).rstrip('\n') + " ==  Not Completed ")
			f.write("\n")
			f.close()
		driver.switch_to.default_content()
		
		# Writing into a file == Extra Space
		# f = open(DOWNLOAD_REPORT_FILENAME, "a+")
		# f.write("\n")
		# f.write("\n")
		# f.close()
		
driver.switch_to.window(window_before)
driver.switch_to.default_content()
logout_iframe = driver.find_element_by_name('frmTop')
driver.switch_to.frame(logout_iframe)
time.sleep(2)
logout = driver.find_element_by_xpath('//html//body//div[1]//table//tbody//tr//td[2]//div//center//table//tbody//tr//td[11]//div//center//p//a')
logout.click()

driver.quit()

