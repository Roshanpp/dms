import os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


from account_details import VALUE, USERNAME, PASSWORD

# Fetching Data from database
value = VALUE

DMS_ACCOUNT_FETCH_URL = 'http://172.25.6.118/extract_account_numner_nodata/' + value

LOG_DIRECTORY = os.path.join(BASE_DIR, 'log')
ACCOUNT_NUMBER_FILE = os.path.join(BASE_DIR, 'log/account_record.txt')


# Download Data Info

LOG_FILENAME = os.path.join(BASE_DIR, 'log/log_file.log')

LOGIN_URL = 'http://172.25.12.28:8080/omnidocs/login.jsp'
#LOGIN = 'http://192.168.100.187:8081/login/?next=/'

ACCOUNT_NUMBER_FILENAME = ACCOUNT_NUMBER_FILE
ACCESS_UPLOAD_REPORT_FILENAME = os.path.join(BASE_DIR, 'log/access_upload_report.txt')
ACCESS_DOWNLOAD_REPORT_FILENAME = os.path.join(BASE_DIR, 'log/access_download_report.txt')

ERROR_UPLOAD_REPORT_FILENAME = os.path.join(BASE_DIR, 'log/error_upload_report.txt')
ERROR_DOWNLOAD_REPORT_FILENAME = os.path.join(BASE_DIR, 'log/error_download_report.txt')

INITIAL_DOWNLOADED_PATH = os.path.join(BASE_DIR, 'Data_Downloaded')
FINAL_DOWNLOAD_PATH = os.path.join(BASE_DIR, 'Final_data_downloaded')

USERNAME = USERNAME
PASSWORD = PASSWORD


# Upload Data Info

DMS_ACCOUNT_UPLOAD_URL = 'http://172.25.6.118/ajax_call_for_files'

DOWNLOADED_PATH = FINAL_DOWNLOAD_PATH

ACCOUNT_NUMBER_FILENAME_UPLOAD = ACCOUNT_NUMBER_FILE
ACCOUNT_TYPE_FILE = os.path.join(BASE_DIR, 'log/account_type.txt')


